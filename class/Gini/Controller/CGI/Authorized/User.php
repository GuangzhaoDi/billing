<?php

namespace Gini\Controller\CGI\Authorized;

class User extends \Gini\Controller\REST {

    function getDefault ($id = 0) {
        $form = $this->form('get');
        $response = [];
        
        if ($id) {
            $connect = a('authorized/user', $id);
        }
        else {
            $connect = those('authorized/user');

            if ($form['authorized']) {
                $connect->whose('authorized_id')->is($form['authorized']);
            }

            if ($form['user']) {
                $user = a('user', ['oid' => $form['user']]);
                $connect->whose('user')->is($user);
            }

            if ($form['sortby'] && $form['order']) {
                $connect->orderBy((string)$form['sortby'], (string)$form['order']);
            }

            if ($form['limit']) {
                $connect->limit((int)$form['limit']);
            }
        }

        if ($connect instanceof \Gini\Those) {
            foreach ($connect as $item) {
                $response[] = [
                    'id' => $item->id,
                    'user' => $item->user->oid,
                    'authorized' => $item->authorized->id,
                    'ctime' => $item->ctime,
                ];
            }
        }
        else if ($connect instanceof \Gini\ORM\Object) {
            $response = [
                'id' => $connect->id,
                'user' => $connect->user->oid,
                'authorized' => $connect->authorized->id,
                'ctime' => $connect->ctime,
            ];
        }
        $res = \Gini\IoC::construct('\Gini\CGI\Response\JSON', $response);
        $logger = \Gini\IoC::construct('\Gini\Logger', 'auth_record');
        $logger->info('方法 [{method}]  返回结果：{res}',[
            'method' => 'GET',
            'res' => J($response)
        ]);
        return $res;
    }

    function postDefault () {
        $form = $this->form('post');

        $response = [
            'error' => [
                'code' => '400',
                'message' => 'Bad Request',
            ]
        ];
        
        if ($form) {
            _G('ME', a('user', ['oid' => $form['me']]));
            $authorized = a('authorized', $form['authorized']);
            $user = a('user', ['oid' => $form['user']]);

            if ($authorized->connect($user)) {
                $connect = a('authorized/user')->whose('user')->is($user)
                    ->whose('authorized')->is($authorized);
                $response = [
                    'id' => $connect->id,
                    'authorized' => $connect->authorized->id,
                    'user' => $connect->user->oid,
                    'ctime' => $connect->ctime,
                ];
            }
            else {
                $response = [
                    'error' => [
                        'code' => '500',
                        'message' => 'Internal Server Error',
                    ]
                ];
            }
        }
        
        if ($response['error']) {
            $status = 'ERROR';
        } else {
            $status = 'SUCCESS';
        }

        $res = \Gini\IoC::construct('\Gini\CGI\Response\JSON', $response);
        $logger = \Gini\IoC::construct('\Gini\Logger', 'auth_user');
        $logger->info('方法 [POST] [{status}] 返回结果 {res}',[
            'status' => $status,
            'res' => J($response)
        ]);
        return $res;
    }

    function deleteDefault ($id) {
        $response = false;
        $connect = a('authorized/user', $id);
        parse_str(file_get_contents('php://input'), $form);

        if ($connect->id && $form) {
            $user = $connect->user;
            $authorized = $connect->authorized;
            _G('ME', a('user', ['oid' => $form['me']]));
            if ($authorized->disconnect($user)) {
                $response = true;
            }
            else {
                $response = [
                    'error' => [
                        'code' => '500',
                        'message' => 'Internal Server Error',
                    ]
                ];
            }
        }

        if ($response['error']) {
            $status = 'ERROR';
        } else {
            $status = 'SUCCESS';
        }

        $res = \Gini\IoC::construct('\Gini\CGI\Response\JSON', $response);
        $logger = \Gini\IoC::construct('\Gini\Logger', 'auth_user');
        $logger->info('方法 [DELETE] [{status}] 返回结果 {res}',[
            'status' => $status,
            'res' => J($response)
        ]);
        return $res;
    }

    function patchDefault($id = 0) {
        parse_str(file_get_contents('php://input'), $form);

        if (!$id) {
            $response = [
                'error' => [
                    'code' => '400',
                    'message' => 'Bad Request',
                ]
            ];
            goto output;
        }

        $authorized = a('authorized', $id);
        if (!$authorized->id) {
            $response = [
                'error' => [
                    'code' => '404',
                    'message' => 'Not Found',
                ]
            ];
            goto output;
        }
        
        if ($form) {
            $props = get_class_vars(get_class($account));
            foreach ($form as $key => $value) {
                if (array_key_exists($key, $props)) {
                    $account->{$key} = $value;
                }
            }

            if ($account->save()) {
                $response = [
                    'id' => $account->id,
                    'fund' => $account->fund->id,
                    'user' => $account->user->id,
                    'lab' => $account->lab->id,
                    'balance' => $account->balance,
                    'total' => $account->total,
                    'ctime' => $account->ctime,
                ];
            }
            else {
                $response = [
                    'error' => [
                        'code' => '500',
                        'message' => 'Internal Server Error',
                    ]
                ];
            }
        }

        output:

        if ($response['error']) {
            $status = 'ERROR';
        } else {
            $status = 'SUCCESS';
        }

        $res = \Gini\IoC::construct('\Gini\CGI\Response\JSON', $response);
        $logger = \Gini\IoC::construct('\Gini\Logger', 'auth_user');
        $logger->info('方法 [PATCH] [{status}] 返回结果 {res}',[
            'status' => $status,
            'res' => J($response)
        ]);
        return $res;
    }

}