<?php

namespace Gini\Controller\CGI\Authorized;

class Record extends \Gini\Controller\REST {

    function getDefault ($id = 0) {
        $form = $this->form('get');
        $response = [];
        
        if ($id) {
            $record = a('authorized/record', $id);
        }
        else if ($form['_object']) {
            $record = a('authorized/record');

            if ($form['operator']) {
                $user = a('user', ['oid' => $form['operator']]);
                $record->whose('operator')->is($user);
            }
            
            if ($form['licensor']) {
                $record->whose('licensor_id')->is($form['licensor']);
            }

            if ($form['type']) {
                $record->whose('type')->is($form['type']);
            }
        }
        else {
            $record = those('authorized/record');

            if ($form['keyword']) {
                $keyword = $form['keyword'];
                $record->whose('licensor')->isIn(
                    those('user')->whose('name')->contains($keyword)
                )->orWhose('authorized')->isIn(
                    those('authorized')->whose('account')->isIn(
                        those('account')->whose('fund')->isIn(
                            those('fund')->whose('dept_name')->contains($keyword)
                            ->orWhose('dept_no')->contains($keyword)
                            ->orWhose('prot_name')->contains($keyword)
                            ->orWhose('prot_no')->contains($keyword)
                        )
                    )
                );
            }
            if ($form['licensor']) {
                $record->whose('licensor')->isIn(
                    those('user')->whose('name')->contains($form['licensor'])
                );
            }
            if (isset($form['only_record'])) {
                $record->whose('licensor_id')->match('=', null);
            }
            if ($form['type'] && $form['type'] != 'ALL_VALUE' && $form['type'] != '全部') {
                $record->whose('type')->is($form['type']);
            }
            if ($form['ctime']) {
                if ($form['ctime'][0]) {
                    $record->whose('ctime')->isGreaterThan($form['ctime'][0]);
                }
                if ($form['ctime'][1]) {
                    $record->whose('ctime')->isLessThan($form['ctime'][1]);
                }
            }
            if ($form['authorized']) {
                $record->whose('authorized')->is(a('authorized', $form['authorized']));
            }
            if ($form['operator']) {
                $user = a('user', ['oid' => $form['operator']]);
                $record->whose('operator')->is($user);
            }

            if ($form['sortby'] && $form['order']) {
                $record->orderBy((string)$form['sortby'], (string)$form['order']);
            }

            $response['total'] = $record->totalCount();
            
            if ($form['limit']) {
                list($start, $per) = $form['limit'];
                $record->limit($start, $per);
            } 
        }
        
        if ($record instanceof \Gini\Those) {
            foreach ($record as $item) {
                $response[] = [
                    'id' => $item->id,
                    'authorized' => $item->authorized->id,
                    'operator' => $item->operator->id,
                    'licensor' => $item->licensor->id,
                    'balance' => $item->balance,
                    'type' => $item->type,
                    'ctime' => $item->ctime,
                ];
            }
        }
        else if ($record instanceof \Gini\ORM\Object) {
            $response = [
                'id' => $record->id,
                'authorized' => $record->authorized->id,
                'operator' => $record->operator->id,
                'licensor' => $record->licensor->id,
                'balance' => $record->balance,
                'type' => $record->type,
                'ctime' => $record->ctime,
            ];
        }
        $res = \Gini\IoC::construct('\Gini\CGI\Response\JSON', $response);
        $logger = \Gini\IoC::construct('\Gini\Logger', 'auth_record');
        $logger->info('方法 [{method}]  返回结果：{res}',[
            'method' => 'GET',
            'res' => J($response)
        ]);
        return $res;
    }

    function postDefault () {
        $form = $this->form('post');
        
        $response = [
            'error' => [
                'code' => '400',
                'message' => 'Bad Request',
            ]
        ];
        
        if ($form) {
            $record = a('authorized/record');
            $record->authorized = a('authorized', $form['authorized']);
            $record->operator = a('user', ['oid' => $form['operator']]);
            if ($form['licensor']) $record->licensor = a('user', ['oid' => $form['licensor']]);
            $record->balance = $form['balance'];
            $record->type = $form['type'];
            if ($record->save()) {
                $response = [
                    'id' => $record->id,
                    'authorized' => $record->authorized->id,
                    'operator' => $record->operator->id,
                    'licensor' => $record->licensor->id,
                    'balance' => $record->balance,
                    'type' => $record->type,
                    'ctime' => $record->ctime,
                ];
            }
            else {
                $response = [
                    'error' => [
                        'code' => '500',
                        'message' => 'Internal Server Error',
                    ]
                ];
            }
        }
        if ($response['error']) {
            $status = 'ERROR';
        } else {
            $status = 'SUCCESS';
        }

        $res = \Gini\IoC::construct('\Gini\CGI\Response\JSON', $response);
        $logger = \Gini\IoC::construct('\Gini\Logger', 'auth_record');
        $logger->info('方法 [POST] [{status}] 返回结果 {res}', [
            'status' => $status,
            'res' => J($response)
        ]);
        return $res;
    }

}