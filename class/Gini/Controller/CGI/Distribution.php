<?php

namespace Gini\Controller\CGI;

class Distribution extends \Gini\Controller\REST {

    function getDefault ($id = 0) {
        $form = $this->form('get');
        $response = [];
        
        if ($id) {
            $distribution = a('distribution', $id);
        }
        else {
            $distribution = those('distribution');
            
            if ($form['equipment']) {
                $distribution->whose('equipment')->isIn($form['equipment']);
            }
            
            if ($form['like_id']) {
                $distribution->whose('id')->contains((int)$form['like_id']);
            }
            if ($form['status'] && $form['status'] != "全部") {
                $distribution->whose('status')->isIn($form['status']);
            }

            if ($form['sortby'] && $form['order']) {
                $distribution->orderBy((string)$form['sortby'], (string)$form['order']);
            }
            if ($form['ctime']) {
                if ($form['ctime'][0]) {
                    $distribution->whose('ctime')->isGreaterThan($form['ctime'][0]);
                }
                if ($form['ctime'][1]) {
                    $distribution->whose('ctime')->isLessThan($form['ctime'][1]);
                }
            }
            $response['total'] = $distribution->totalCount();
            
            if ($form['limit']) {
                list($start, $per) = $form['limit'];
                $distribution->limit($start, $per);
            } 
        }

        if ($distribution instanceof \Gini\Those) {
            foreach ($distribution as $item) {
                $response[] = [
                    'id' => $item->id,
                    'admin' => $item->admin,
                    'admin_scale' => $item->admin_scale,
                    'manager' => $item->manager,
                    'manager_scale' => $item->manager_scale,
                    'school' => $item->school,
                    'school_scale' => $item->school_scale,
                    'balance' => $item->balance,
                    'equipment' => $item->equipment,
                    'status' => $item->status,
                    'reviewe' => $item->reviewe->id,
                    'complete' => $item->complete->id,
                    'reserve' => $distribution->reserve,
                    'ctime' => $item->ctime,
                ];
            }
        }
        else if ($distribution instanceof \Gini\ORM\Object) {
            $response = [
                'id' => $distribution->id,
                'admin' => $distribution->admin,
                'admin_scale' => $distribution->admin_scale,
                'manager' => $distribution->manager,
                'manager_scale' => $distribution->manager_scale,
                'school' => $distribution->school,
                'school_scale' => $distribution->school_scale,
                'balance' => $distribution->balance,
                'equipment' => $distribution->equipment,
                'status' => $distribution->status,
                'reviewe' => $distribution->reviewe->oid,
                'complete' => $distribution->complete->oid,
                'reserve' => $distribution->reserve,
                'ctime' => $distribution->ctime,
            ];
        }
        
        $res = \Gini\IoC::construct('\Gini\CGI\Response\JSON', $response);
        $logger = \Gini\IoC::construct('\Gini\Logger', 'distribution');
        $logger->info('方法 [{method}]  返回结果：{res}',[
            'method' => 'GET',
            'res' => J($response)
        ]);
        return $res;
    }

    function putDefault ($id = 0) {
        parse_str(file_get_contents('php://input'), $form);

        if (!$id) {
            $response = [
                'error' => [
                    'code' => '400',
                    'message' => 'Bad Request',
                ]
            ];
            goto output;
        }

        $distribution = a('distribution', $id);
        if (!$distribution->id) {
            $response = [
                'error' => [
                    'code' => '404',
                    'message' => 'Not Found',
                ]
            ];
            goto output;
        }
        
        if ($form) {
            $distribution->status = $form['status'];
            $distribution->manager = $form['manager'];
            if ($form['reviewe']) $distribution->reviewe = a('user', ['oid' => $form['reviewe']]);
            if ($form['complete']) $distribution->complete = a('user', ['oid' => $form['complete']]);
            if ($distribution->save()) {
                $response = [
                    'id' => $distribution->id,
                    'admin' => $distribution->admin,
                    'admin_scale' => $distribution->admin_scale,
                    'manager' => $distribution->manager,
                    'manager_scale' => $distribution->manager_scale,
                    'school' => $distribution->school,
                    'school_scale' => $distribution->school_scale,
                    'balance' => $distribution->balance,
                    'equipment' => $distribution->equipment,
                    'status' => $distribution->status,
                    'reviewe' => $distribution->reviewe->oid,
                    'complete' => $distribution->complete->oid,
                    'reserve' => $distribution->reserve,
                    'ctime' => $distribution->ctime,
                ];
            }
            else {
                $response = [
                    'error' => [
                        'code' => '500',
                        'message' => 'Internal Server Error',
                    ]
                ];
            }
        }

        output:

        if ($response['error']) {
            $status = 'ERROR';
        } else {
            $status = 'SUCCESS';
        }

        $res = \Gini\IoC::construct('\Gini\CGI\Response\JSON', $response);
        $logger = \Gini\IoC::construct('\Gini\Logger', 'distribution');
        $logger->info('方法 [PUT] [{status}] 返回结果 {res}',[
            'status' => $status,
            'res' => J($response)
        ]);
        return $res;
    }

    function patchDefault($id = 0) {
        parse_str(file_get_contents('php://input'), $form);

        if (!$id) {
            $response = [
                'error' => [
                    'code' => '400',
                    'message' => 'Bad Request',
                ]
            ];
            goto output;
        }

        $authorized = a('authorized', $id);
        if (!$authorized->id) {
            $response = [
                'error' => [
                    'code' => '404',
                    'message' => 'Not Found',
                ]
            ];
            goto output;
        }
        
        if ($form) {
            $props = get_class_vars(get_class($authorized));
            foreach ($form as $key => $value) {
                if (array_key_exists($key, $props)) {
                    $authorized->{$key} = $value;
                }
            }

            if ($authorized->save()) {
                $response = [
                    'id' => $authorized->id,
                    'balance' => $authorized->balance,
                    'ctime' => $authorized->ctime,
                ];

                if ($form['diff']) {
                    $record = a('authorized/record');
                    $record->operator = a('user', ['oid' => $form['me']]);
                    $record->balance = abs($form['diff']);
                    $record->type = $form['diff'] > 0 ? \Gini\ORM\Authorized\Record::TYPE_PUT : \Gini\ORM\Authorized\Record::TYPE_DEDUCT;
                    $record->authorized = $authorized;
                    $record->save();
                }
            }
            else {
                $response = [
                    'error' => [
                        'code' => '500',
                        'message' => 'Internal Server Error',
                    ]
                ];
            }
        }

        output:

        if ($response['error']) {
            $status = 'ERROR';
        } else {
            $status = 'SUCCESS';
        }

        $res = \Gini\IoC::construct('\Gini\CGI\Response\JSON', $response);
        $logger = \Gini\IoC::construct('\Gini\Logger', 'distribution');
        $logger->info('方法 [PATCH] [{status}] 返回结果 {res}',[
            'status' => $status,
            'res' => J($response)
        ]);
        return $res;
    }

}