<?php

namespace Gini\Controller\CGI;

class Account extends \Gini\Controller\REST {

    function getDefault ($id = 0) {
        $form = $this->form('get');
        $response = [];

        if ($id) {
            $account = a('account', $id);
        }
        else {
            $account = those('account');

            if ($form['keyword']) {
                $keyword = $form['keyword'];
                $account->whose('fund')->isIn(
                    those('fund')->whose('dept_name')->contains($keyword)
                    ->orWhose('dept_no')->contains($keyword)
                    ->orWhose('prot_name')->contains($keyword)
                    ->orWhose('prot_no')->contains($keyword)
                );
            } 

            if ($form['like_id']) {
                $account->whose('id')->contains((int)$form['like_id']);
            }
            if ($form['dept_no'] || $form['dept_name'] || $form['prot_no'] || $form['prot_name']) {
                $fund = those('fund');
                if ($form['dept_no']) {
                    $fund->whose('dept_no')->contains($form['dept_no']);
                }
                if ($form['dept_name']) {
                    $fund->whose('dept_name')->contains($form['dept_name']);
                }
                if ($form['prot_no']) {
                        $fund->whose('prot_no')->contains($form['prot_no']);
                }
                if ($form['prot_name']) {
                    $fund->whose('prot_name')->contains($form['prot_name']);
                }
                $account->whose('fund')->isIn(
                    $fund
                );
            }
            if ($form['user']) {
                $user = a('user', ['oid' => $form['user']]);
                $account->whose('user')->is($user);
            }        
            
            if ($form['lab']) {
                $lab = a('lab', ['oid' => $form['lab']]);
                $account->whose('lab')->is($lab);
            }

            if ($form['fund']) {
                $account->whose('fund_id')->is((int)$form['fund']);
            }

            if ($form['sortby'] && $form['order']) {
                $account->orderBy((string)$form['sortby'], (string)$form['order']);
            }

            $response['total'] = $account->totalCount();
            
            if ($form['limit']) {
                list($start, $per) = $form['limit'];
                $account->limit($start, $per);
            }
        }

        if ($account instanceof \Gini\Those) {
            foreach ($account as $item) {
                $response[] = [
                    'id' => $item->id,
                    'fund' => $item->fund->id,
                    'user' => $item->user->id,
                    'lab' => $item->lab->id,
                    'balance' => $item->balance,
                    'total' => $item->total,
                    'ctime' => $item->ctime,
                ];
            }
        }
        else if ($account instanceof \Gini\ORM\Object) {
            $response = [
                'id' => $account->id,
                'fund' => $account->fund->id,
                'user' => $account->user->id,
                'lab' => $account->lab->id,
                'balance' => $account->balance,
                'total' => $account->total,
                'ctime' => $account->ctime,
            ];
        }
        
        $res = \Gini\IoC::construct('\Gini\CGI\Response\JSON', $response);
        $logger = \Gini\IoC::construct('\Gini\Logger', 'account');
        $logger->info('方法 [{method}]  返回结果：{res}',[
            'method' => 'GET',
            'res' => J($response)
        ]);
        return $res;
    }

    function postDefault () {
        $form = $this->form('post');

        $response = [
            'error' => [
                'code' => '400',
                'message' => 'Bad Request',
            ]
        ];
        
        if ($form) {
            $user = a('user', ['oid' => $form['user']]);
            $lab = a('lab', ['oid' => $form['lab']]);
            $account = a('account');
            $account->fund_id = $form['fund'];
            $account->user = $user;
            $account->lab = $lab;
            $account->balance = $form['balance'];
            $account->total = $form['total'];
            if ($account->save()) {
                $authorized = a('authorized');
                $authorized->account = $account;
                $authorized->type = \Gini\ORM\Authorized::TYPE_AUTO;
                $authorized->save();

                $response = [
                    'id' => $account->id,
                    'fund' => $account->fund->id,
                    'user' => $account->user->id,
                    'lab' => $account->lab->id,
                    'balance' => $account->balance,
                    'total' => $account->total,
                    'ctime' => $account->ctime,
                ];
            }
            else {
                $response = [
                    'error' => [
                        'code' => '500',
                        'message' => 'Internal Server Error',
                    ]
                ];
            }
        }
        
        if ($response['error']) {
            $status = 'ERROR';
        } else {
            $status = 'SUCCESS';
        }

        $res = \Gini\IoC::construct('\Gini\CGI\Response\JSON', $response);
        $logger = \Gini\IoC::construct('\Gini\Logger', 'account');
        $logger->info('方法 [POST] [{status}] 返回结果 {res}',[
            'status' => $status,
            'res' => J($response)
        ]);
        return $res;
    }

    function patchDefault($id = 0) {
        parse_str(file_get_contents('php://input'), $form);

        if (!$id) {
            $response = [
                'error' => [
                    'code' => '400',
                    'message' => 'Bad Request',
                ]
            ];
            goto output;
        }

        $account = a('account', $id);
        if (!$account->id) {
            $response = [
                'error' => [
                    'code' => '404',
                    'message' => 'Not Found',
                ]
            ];
            goto output;
        }
        
        if ($form) {
            $props = get_class_vars(get_class($account));
            foreach ($form as $key => $value) {
                if (array_key_exists($key, $props)) {
                    $account->{$key} = $value;
                }
            }

            if ($account->save()) {
                $response = [
                    'id' => $account->id,
                    'fund' => $account->fund->id,
                    'user' => $account->user->id,
                    'lab' => $account->lab->id,
                    'balance' => $account->balance,
                    'total' => $account->total,
                    'ctime' => $account->ctime,
                ];
            }
            else {
                $response = [
                    'error' => [
                        'code' => '500',
                        'message' => 'Internal Server Error',
                    ]
                ];
            }
        }

        output:
        if ($response['error']) {
            $status = 'ERROR';
        } else {
            $status = 'SUCCESS';
        }

        $res = \Gini\IoC::construct('\Gini\CGI\Response\JSON', $response);
        $logger = \Gini\IoC::construct('\Gini\Logger', 'account');
        $logger->info('方法 [PATCH] [{status}] 返回结果 {res}',[
            'status' => $status,
            'res' => J($response)
        ]);
        return $res;
    }

}