<?php

namespace Gini\Controller\CGI;

class Fund extends \Gini\Controller\REST {

    function getDefault ($id = 0) {
        $form = $this->form('get');
        if ($id) {
            $fund = a('fund', $id);
        }
        else if ($form['ref']) {
            if ($form['dept'] && $form['prot']) {
                $response = m('fund')->balance($form['ref'], $form['dept'], $form['prot']);
            }
            else {
                $response = m('fund')->get($form['ref']);
            }
        }
        else if ($form['dept'] && $form['prot']) {
            $fund = a('fund', ['dept_no' => $form['dept'], 'prot_no' => $form['prot']]);
        }
        else {
            $fund = those('fund');

            if ($form['keyword']) {
                $keyword = $form['keyword'];
                $fund->whose('dept_name')->contains($keyword)
                    ->orWhose('dept_no')->contains($keyword)
                    ->orWhose('prot_name')->contains($keyword)
                    ->orWhose('prot_no')->contains($keyword);
            }
            if ($form['like_id']) {
                $fund->whose('id')->contains((int)$form['like_id']);
            }
            if ($form['dept_no']) {
                $fund->whose('dept_no')->contains($form['dept_no']);
            }
            if ($form['dept_name']) {
                $fund->whose('dept_name')->contains($form['dept_name']);
            }
            if ($form['prot_no']) {
                    $fund->whose('prot_no')->contains($form['prot_no']);
            }
            if ($form['prot_name']) {
                $fund->whose('prot_name')->contains($form['prot_name']);
            }

            if ($form['ctime']) {
                if ($form['ctime'][0]) {
                    $fund->whose('ctime')->isGreaterThan($form['ctime'][0]);
                }
                if ($form['ctime'][1]) {
                    $fund->whose('ctime')->isLessThan($form['ctime'][1]);
                }
            }
            if ($form['lab']) {
                $lab = a('lab', ['oid' => $form['lab']]);
                $ids = $account = those('account')->whose('lab')->is($lab)->get('fund_id');
                $fund->whose('id')->isIn($ids);
            }

            if ($form['ids']) {
                $fund->whose('id')->isIn($form['ids']);
            }

            if ($form['sortby'] && $form['order']) {
                if (is_array($form['sortby'])) {
                    foreach ($form['sortby'] as $index => $sort) {
                        $fund->orderBy((string)$sort, $form['order'][$index]);
                    }
                }
                else {
                    $fund->orderBy((string)$form['sortby'], (string)$form['order']);
                }
            }

            $response['total'] = $fund->totalCount();
            
            if ($form['limit']) {
                list($start, $per) = $form['limit'];
                $fund->limit($start, $per);
            } 
        }

        if ($fund instanceof \Gini\ORM\Object) {
            $response = [
                'id' => $fund->id,
                'dept_name' => $fund->dept_name,
                'dept_no' => $fund->dept_no,
                'prot_name' => $fund->prot_name,
                'prot_no' => $fund->prot_no,
                'ctime' => $fund->ctime,
            ];
        }
        else if ($fund instanceof \Gini\Those) {
            foreach ($fund as $item) {
                $response[] = [
                    'id' => $item->id,
                    'dept_name' => $item->dept_name,
                    'dept_no' => $item->dept_no,
                    'prot_name' => $item->prot_name,
                    'prot_no' => $item->prot_no,
                    'ctime' => $item->ctime,
                ];
            }
        }
        
        $res = \Gini\IoC::construct('\Gini\CGI\Response\JSON', $response);
        $logger = \Gini\IoC::construct('\Gini\Logger', 'fund');
        $logger->info('方法 [GET]  返回结果 {res}',[
            'res' => J($response)
        ]);
        return $res;
    }

    function postDefault () {
        $form = $this->form('post');

        $response = [
            'error' => [
                'code' => '400',
                'message' => 'Bad Request',
            ]
        ];

        if ($form) {
            $fund = a('fund');
            $fund->dept_name = $form['dept_name'];
            $fund->dept_no = $form['dept_no'];
            $fund->prot_name = $form['prot_name'];
            $fund->prot_no = $form['prot_no'];
            if ($fund->save()) {
                $response = [
                    'id' => $fund->id,
                    'dept_name' => $fund->dept_name,
                    'dept_no' => $fund->dept_no,
                    'prot_name' => $fund->prot_name,
                    'prot_no' => $fund->prot_no,
                    'ctime' => $fund->ctime,
                ];
            }
            else {
                $response = [
                    'error' => [
                        'code' => '500',
                        'message' => 'Internal Server Error',
                    ]
                ];
            }
        }
        
        if ($response['error']) {
            $status = 'ERROR';
        } else {
            $status = 'SUCCESS';
        }
        $res = \Gini\IoC::construct('\Gini\CGI\Response\JSON', $response);
        $logger = \Gini\IoC::construct('\Gini\Logger', 'fund');
        $logger->info('方法 [POST] [{status}] 返回结果 {res}',[
            'status' => $status,
            'res' => J($response)
        ]);
        return $res;
    }

    function putDefault() {
        parse_str(file_get_contents('php://input'), $form);
        $response = [
            'error' => [
                'code' => '400',
                'message' => 'Bad Request',
            ]
        ];
        $logs['status'] = '';
        $logs['id'] = [];

        if ($form['action'] && $form['action'] == 'unfreeze') {
            if ($form['account_id']) {
                if (is_numeric($form['account_id'])) {
                    $account = a('account', $form['account_id']);
                    if (!$account->id || !is_numeric($form['money']) || $form['money'] > $account->balance) {
                        $response = [
                            'error' => [
                                'code' => '500',
                                'message' => 'Internal Server Error',
                            ]
                        ];
                        goto output;
                    }
                    $data = [];
                    $tmp['BMBH'] = $account->fund->dept_no;
                    $tmp['XMBH'] = $account->fund->prot_no;
                    $tmp['JE']   = $form['money'];
                    $tmp['LSH']  = 'LS'. $account->id;
                    $tmp['GZH'] = $account->lab->owner_id;
                    $tmp['RQ'] = date('Y-m-d', strtotime($account->ctime));
                    $tmp['GZH'] = $account->lab->owner_id;
                    $tmp['BZ'] = '批量解冻';
                    $tmp['XTLB'] = 2;
                    $data[] = $tmp;
                    $results = m('fund')->unfreeze($data);
                    if ($results == 1) {
                        $logs['status'] = T('远端接口返回成功');
                       
                        $account->total = $account->total - $form['money'];
                        $account->balance = $account->balance - $form['money'];
                        if ($account->save()) {

                            $transaction = a('transaction');
                            $transaction->account = $account;
                            $transaction->authorized = null;
                            $transaction->user_id = $form['user']->oid;
                            $transaction->amount = 0 - $leave_money[$key]['remove_money'];
                            $transaction->transfer = true;
                            $transaction->source = 'szht';

                            if ($transaction->save()) {
                                $logger = \Gini\IoC::construct('\Gini\Logger', 'fund');
                                $logger->info('用户[{userId}]{userName} 账户[{accountId}]充值给课题组{accountLab}金额{balance}', [
                                    'userId' => $form['user']['oid'],
                                    'userName' => $form['user']['name'],
                                    'accountId' => $transaction->account,
                                    'accountLab' => $account->lab,
                                    'balance' => $transaction->amount,
                                ]);
                            }
                            else {
                                $logger = \Gini\IoC::construct('\Gini\Logger', 'fund');
                                $logger->error('用户[{userId}]{userName} [ERROR]账户[{accountId}]解冻{accountLab}金额{balance} 失败', [
                                    'userId' => $form['user']['oid'],
                                    'userName' => $form['user']['name'],
                                    'accountId' => $transaction->account,
                                    'accountLab' => $account->lab,
                                    'balance' => $transaction->amount,
                                ]);
                                $response = [
                                    'error' => [
                                        'code' => '500',
                                        'message' => '流水写入失败',
                                    ]
                                ];
                                goto output;
                            }

                            $logs['id'] = $account->id .':'. T('成功');
                        } else {
                            $logs['id'] = $account->id . ':'. T('失败');
                        }
                        $response = [
                            'fund_id' => $form['fund_id'],
                            'lab_id' => $form['lab_id'],
                            'success' => implode(',', $logs['id'])
                        ];
                    } else {
                        $logs['status'] = T('远端接口返回失败');
                         $response = [
                            'error' => [
                                'code' => '500',
                                'message' => T('远端接口返回失败'),
                            ]
                        ];
                    }
                }
            } elseif (is_numeric($form['fund_id']) && is_numeric($form['lab_id']) && is_numeric($form['money'])) {
                $accounts = those('account')-> whose('fund_id')->is($form['fund_id'])
                    ->andWhose('lab_id')->is($form['lab_id'])->andWhose('balance')->isGreaterThan(0)->orderBy('balance');
                $data = [];
                $leave_money = [];
                $total_money = $form['money'];
                $unfreeze_money = $total_money;
                foreach ($accounts as $key => $account) {
                    if ($unfreeze_money > 0) {
                        if ($account->balance >= $unfreeze_money) {
                            $tmp_money = $unfreeze_money;
                            $unfreeze_money = 0;
                        } else {
                            $tmp_money = $account->balance;
                            $unfreeze_money = $unfreeze_money - $tmp_money;
                        }
                    } else {
                        break;
                    }
                    $tmp['BMBH'] = $account->fund->dept_no;
                    $tmp['XMBH'] = $account->fund->prot_no;
                    $tmp['JE']   = $tmp_money;
                    $tmp['LSH']  = 'LS'. $account->id;
                    $tmp['GZH'] = $account->lab->owner_id;
                    $tmp['RQ'] = date('Y-m-d', strtotime($account->ctime));
                    $tmp['GZH'] = $account->lab->owner_id;
                    $tmp['BZ'] = '批量解冻';
                    $tmp['XTLB'] = 2;
                    $data[] = $tmp;
                    $leave_money[$key] = [
                        'leave_money' => $account->balance - $tmp_money,
                        'remove_money' => $tmp_money
                    ];
                }
                $results = m('fund')->unfreeze($data);
                if ($results == 1) {
                    $logs['status'] = T('远端接口返回成功');
                   
                    foreach ($accounts as $key => $account) {
                        if (!array_key_exists($key, $leave_money)) {
                            continue;
                        }
                        $account->total = $account->total - $leave_money[$key]['remove_money'];
                        $account->balance = $leave_money[$key]['leave_money'];
                        if ($account->save()) {

                            $transaction = a('transaction');
                            $transaction->account = $account;
                            $transaction->authorized = null;
                            $transaction->user_id = $form['user']['oid'];
                            $transaction->amount = 0 - $leave_money[$key]['remove_money'];
                            $transaction->transfer = true;
                            $transaction->source = 'szht';

                            if ($transaction->save()) {
                                $logger = \Gini\IoC::construct('\Gini\Logger', 'fund');
                                $logger->info('用户[{userId}]{userName} 账户[{accountId}]充值给课题组{accountLab}金额{balance}', [
                                    'userId' => $form['user']['oid'],
                                    'userName' => $form['user']['name'],
                                    'accountId' => $transaction->account,
                                    'accountLab' => $account->lab,
                                    'balance' => $transaction->amount,
                                ]);
                            }
                            else {
                                $logger = \Gini\IoC::construct('\Gini\Logger', 'fund');
                                $logger->error('用户[{userId}]{userName} [ERROR]账户[{accountId}]解冻{accountLab}金额{balance} 失败', [
                                    'userId' => $form['user']['oid'],
                                    'userName' => $form['user']['name'],
                                    'accountId' => $transaction->account,
                                    'accountLab' => $account->lab,
                                    'balance' => $transaction->amount,
                                ]);
                                $response = [
                                    'error' => [
                                        'code' => '500',
                                        'message' => '流水写入失败',
                                    ]
                                ];
                                goto output;
                            }

                            $logs['id'] = $account->id .':'. T('成功');
                        } else {
                            $logs['id'] = $account->id . ':'. T('失败');
                        }
                    }
                    $response = [
                        'fund_id' => $form['fund_id'],
                        'lab_id' => $form['lab_id'],
                        'success' => implode(',', $logs['id'])
                    ];
                } else {
                    $logs['status'] = T('远端接口返回失败');
                     $response = [
                        'error' => [
                            'code' => '500',
                            'message' => T('远端接口返回失败'),
                        ]
                    ];
                }
            }
            else {
                $response = [
                    'error' => [
                        'code' => '500',
                        'message' => 'Internal Server Error',
                    ]
                ];
                goto output;
            }
        }

        output:
        if ($response['error']) {
            $status = 'ERROR';
        } else {
            $status = 'SUCCESS';
        }
        $res = \Gini\IoC::construct('\Gini\CGI\Response\JSON', $response);
        
        $logger = \Gini\IoC::construct('\Gini\Logger', 'fund');
        $logger->info('方法 [PUT] [{status}] 中间结果：{tmpres} 返回结果 {res} ',[
            'tmpres' => $logs['status'] . ':'. implode(',', $logs['id']),
            'status' => $status,
            'res' => J($response)
        ]);
        return $res;
    }
}
