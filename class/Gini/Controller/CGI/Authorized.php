<?php

namespace Gini\Controller\CGI;

class Authorized extends \Gini\Controller\REST {

    function getDefault ($id = 0) {
        $form = $this->form('get');
        $response = [];
        
        if ($id) {
            $authorized = a('authorized', $id);
        }
        else {
            $authorized = those('authorized');

            if ($form['keyword']) {
                $keyword = $form['keyword'];
                $authorized->whose('account')->isIn(
                    those('account')->whose('fund_id')->isIn(
                        those('fund')->whose('dept_name')->contains($keyword)
                        ->orWhose('dept_no')->contains($keyword)
                        ->orWhose('prot_name')->contains($keyword)
                        ->orWhose('prot_no')->contains($keyword)->get('id')
                    )
                );
            }
            
            if ($form['like_id']) {
                $authorized->whose('id')->contains((int)$form['like_id']);
            }

            if ($form['dept_no'] || $form['dept_name'] || $form['prot_no'] || $form['prot_name']) {
                $fund = those('fund');
                if ($form['dept_no']) {
                    $fund->whose('dept_no')->contains($form['dept_no']);
                }
                if ($form['dept_name']) {
                    $fund->whose('dept_name')->contains($form['dept_name']);
                }
                if ($form['prot_no']) {
                        $fund->whose('prot_no')->contains($form['prot_no']);
                }
                if ($form['prot_name']) {
                    $fund->whose('prot_name')->contains($form['prot_name']);
                }
                $authorized->whose('account')->isIn(
                    those('account')->whose('fund')->isIn(
                        $fund
                    )
                );
            }
            
            if ($form['user']) {
                $user = a('user', ['oid' => $form['user']]);
                $authorized->whose('id')->isIn(
                    those('authorized/user')->whose('user')->is($user)->get('authorized_id')
                );
            }

            if ($form['account']) {
                if (is_array($form['account'])) {
                    $authorized->whose('account_id')->isIn($form['account']);
                }
                else {
                    $authorized->whose('account_id')->is($form['account']);
                }
            }

            if ($form['type']) {
                $authorized->andWhose('type')->isIn($form['type']);
            }

            if ($form['sortby'] && $form['order']) {
                $authorized->orderBy((string)$form['sortby'], (string)$form['order']);
            }

            if ($form['compare']) {
                list($key, $condition, $value) = $form['compare'];
                switch ($condition) {
                    case '>':
                        $method = 'isGreaterThan';
                        break;
                }
                $authorized->whose($key)->$method($value);
            }

            $response['total'] = $authorized->totalCount();
            
            if ($form['limit']) {
                list($start, $per) = $form['limit'];
                $authorized->limit($start, $per);
            }
        }

        if ($authorized instanceof \Gini\Those) {
            foreach ($authorized as $item) {
                $response[] = [
                    'id' => $item->id,
                    'fund' => $item->account->fund->id,
                    'account' => $item->account->id,
                    'balance' => $item->balance,
                    'type' => $item->type,
                    'ctime' => $item->ctime,
                ];
            }
        }
        else if ($authorized instanceof \Gini\ORM\Object) {
            $response = [
                'id' => $authorized->id,
                'fund' => $authorized->account->fund->id,
                'account' => $authorized->account->id,
                'balance' => $authorized->balance,
                'type' => $authorized->type,
                'ctime' => $authorized->ctime,
            ];
        }
        
        $res = \Gini\IoC::construct('\Gini\CGI\Response\JSON', $response);
        $logger = \Gini\IoC::construct('\Gini\Logger', 'authorized');
        $logger->info('方法 [{method}]  返回结果：{res}',[
            'method' => 'GET',
            'res' => J($response)
        ]);
        return $res;
    }

    function postDefault () {
        $form = $this->form('post');
        
        $response = [
            'error' => [
                'code' => '400',
                'message' => 'Bad Request',
            ]
        ];
        
        if ($form) {
            $authorized = a('authorized');
            $authorized->account = a('account', $form['account']);
            $authorized->balance = $form['balance'];
            if ($authorized->save()) {
                $response = [
                    'id' => $authorized->id,
                    'account' => $authorized->fund->id,
                    'balance' => $authorized->user->id,
                    'ctime' => $authorized->ctime,
                ];
            }
            else {
                $response = [
                    'error' => [
                        'code' => '500',
                        'message' => 'Internal Server Error',
                    ]
                ];
            }
        }
        
        if ($response['error']) {
            $status = 'ERROR';
        } else {
            $status = 'SUCCESS';
        }

        $res = \Gini\IoC::construct('\Gini\CGI\Response\JSON', $response);
        $logger = \Gini\IoC::construct('\Gini\Logger', 'authorized');
        $logger->info('方法 [POST] [{status}] 返回结果 {res}',[
            'status' => $status,
            'res' => J($response)
        ]);
        return $res;
    }

    function putDefault ($id = 0) {
        parse_str(file_get_contents('php://input'), $form);

        if (!$id) {
            $response = [
                'error' => [
                    'code' => '400',
                    'message' => 'Bad Request',
                ]
            ];
            goto output;
        }

        $authorized = a('authorized', $id);
        if (!$authorized->id) {
            $response = [
                'error' => [
                    'code' => '404',
                    'message' => 'Not Found',
                ]
            ];
            goto output;
        }
        
        if ($form) {
            $diff = $form['balance'] - $authorized->balance;
            $authorized->account = a('account', $form['account']);
            $authorized->balance = $form['balance'];
            if ($authorized->save()) {
                $response = [
                    'id' => $authorized->id,
                    'account' => $authorized->fund->id,
                    'balance' => $authorized->user->id,
                    'ctime' => $authorized->ctime,
                ];
            }
            else {
                $response = [
                    'error' => [
                        'code' => '500',
                        'message' => 'Internal Server Error',
                    ]
                ];
            }
        }

        output:
        
        if ($response['error']) {
            $status = 'ERROR';
        } else {
            $status = 'SUCCESS';
        }

        $res = \Gini\IoC::construct('\Gini\CGI\Response\JSON', $response);
        $logger = \Gini\IoC::construct('\Gini\Logger', 'authorized');
        $logger->info('方法 [PUT] [{status}] 返回结果 {res}',[
            'status' => $status,
            'res' => J($response)
        ]);
        return $res;
    }

    function patchDefault($id = 0) {
        parse_str(file_get_contents('php://input'), $form);

        if (!$id) {
            $response = [
                'error' => [
                    'code' => '400',
                    'message' => 'Bad Request',
                ]
            ];
            goto output;
        }

        $authorized = a('authorized', $id);
        if (!$authorized->id) {
            $response = [
                'error' => [
                    'code' => '404',
                    'message' => 'Not Found',
                ]
            ];
            goto output;
        }
        
        if ($form) {
            $props = get_class_vars(get_class($authorized));
            foreach ($form as $key => $value) {
                if (array_key_exists($key, $props)) {
                    $authorized->{$key} = $value;
                }
            }

            if ($authorized->save()) {
                if ($form['diff']) {
                    $record = a('authorized/record');
                    $record->operator = a('user', ['oid' => $form['me']]);
                    $record->balance = abs($form['diff']);
                    $record->type = $form['diff'] > 0 ? \Gini\ORM\Authorized\Record::TYPE_PUT : \Gini\ORM\Authorized\Record::TYPE_DEDUCT;
                    $record->authorized = $authorized;
                    $record->save();
                }
                
                $response = [
                    'id' => $authorized->id,
                    'fund' => $authorized->account->fund->id,
                    'account' => $authorized->account->id,
                    'balance' => $authorized->balance,
                    'ctime' => $authorized->ctime,
                ];
            }
            else {
                $response = [
                    'error' => [
                        'code' => '500',
                        'message' => 'Internal Server Error',
                    ]
                ];
            }
        }

        output:

        if ($response['error']) {
            $status = 'ERROR';
        } else {
            $status = 'SUCCESS';
        }

        $res = \Gini\IoC::construct('\Gini\CGI\Response\JSON', $response);
        $logger = \Gini\IoC::construct('\Gini\Logger', 'authorized');
        $logger->info('方法 [PATCH] [{status}] 返回结果 {res}',[
            'status' => $status,
            'res' => J($response)
        ]);
        return $res;
    }

}