<?php

namespace Gini\Controller\CGI;

class Transaction extends \Gini\Controller\REST {

    function getDefault ($id = 0) {
        $form = $this->form('get');
        $response = [];

        if ($id) {
            $transaction = a('transaction', $id);
        }
        else if ($form['_object']) {
            $transaction = a('transaction');
                
            if ($form['account']) {
                $transaction->whose('account_id')->is((int)$form['account']);
            }
                
            if ($form['source']) {
                $transaction->whose('source')->is($form['source']);
            }

            if ($form['object']) {
                $transaction->whose('object')->is($form['object']);
            }

            if ($form['authorized']) {
                $transaction->whose('authorized_id')->is($form['authorized']);
            }
        }
        else {
            if ($form['object'] && $form['source']) {
                $transaction = a('transaction')->whose('object')->is($form['object'])
                    ->andWhose('source')->is($form['source']);
                if ($form['authorized']) {
                    $authorized = a('authorized', $form['authorized']);
                    $transaction->whose('authorized')->is($authorized);
                }
            }
            else {
                $transaction = those('transaction');

                if ($form['keyword']) {
                    $keyword = $form['keyword'];
                    $transaction->whose('user')->isIn(
                        those('user')->whose('name')->contains($keyword)
                    )->orWhose('account')->isIn(
                        those('account')->whose('fund')->isIn(
                            those('fund')->whose('dept_name')->contains($keyword)
                            ->orWhose('dept_no')->contains($keyword)
                            ->orWhose('prot_name')->contains($keyword)
                            ->orWhose('prot_no')->contains($keyword)
                        )
                    );
                }
                
                if ($form['like_id']) {
                    $transaction->whose('id')->contains((int)$form['like_id']);
                }

                if ($form['lab']) {
                    $transaction->whose('account')->isIn(
                        those('account')->whose('lab')->isIn(
                            those('lab')->whose('name')->contains($form['lab'])
                        )
                    );
                    
                }

                if ($form['dept_no'] || $form['dept_name'] || $form['prot_no'] || $form['prot_name']) {
                    $fund = those('fund');
                    if ($form['dept_no']) {
                        $fund->whose('dept_no')->contains($form['dept_no']);
                    }
                    if ($form['dept_name']) {
                        $fund->whose('dept_name')->contains($form['dept_name']);
                    }
                    if ($form['prot_no']) {
                         $fund->whose('prot_no')->contains($form['prot_no']);
                    }
                    if ($form['prot_name']) {
                        $fund->whose('prot_name')->contains($form['prot_name']);
                    }
                    $transaction->whose('account')->isIn(
                        those('account')->whose('fund')->isIn(
                            $fund
                        )
                    );
                }

                if ($form['type']) {
                    switch ($form['type']) {
                        case 1 :
                            $transaction->whose('amount')->isGreaterThan(0)
                                ->andWhose('transfer')->is(1);
                            break;
                        case 2 :
                            $transaction->whose('amount')->isLessThan(0)
                                ->andWhose('transfer')->is(1);
                            break;
                        case 3 :
                            $transaction->whose('amount')->isLessThan(0)
                                ->andWhose('transfer')->is(0);
                            break;
                    }
                }

                if ($form['ctime']) {
                    if ($form['ctime'][0]) {
                        $transaction->whose('ctime')->isGreaterThan($form['ctime'][0]);
                    }
                    if ($form['ctime'][1]) {
                        $transaction->whose('ctime')->isLessThan($form['ctime'][1]);
                    }
                }

                if ($form['account']) {
                    $transaction->whose('account_id')->is((int)$form['account']);
                }
                
                if ($form['source']) {
                    $transaction->whose('source')->is($form['source']);
                }
                
                if ($form['distribution']) {
                    $transaction->whose('distribution')->is(a('distribution', $form['distribution']));
                }

                if ($form['user']) {
                    $user = a('user', ['oid' => $form['user']]);
                    $transaction->whose('user')->is($user);
                }

                if ($form['user_name']) {
                    $user = a('user')->whose('name')->contains($form['user_name']);
                    $transaction->whose('user')->is($user);
                }
                if ($form['pi']) {
                    $pi = a('user', ['oid' => $form['pi']]);
                    $transaction->whose('account')->isIn(
                        those('account')->whose('lab')->is($pi->lab)
                    );
                }

                if ($form['transfer']) {
                    $transaction->whose('transfer')->is((int)$form['transfer']);
                }

                if ($form['sortby'] && $form['order']) {
                    $transaction->orderBy((string)$form['sortby'], (string)$form['order']);
                }

                $response['total'] = $transaction->totalCount();
                
                if ($form['limit']) {
                    list($start, $per) = $form['limit'];
                    $transaction->limit($start, $per);
                }
            }
        }

        if ($transaction instanceof \Gini\Those) {
            foreach ($transaction as $item) {
                $response[] = [
                    'id' => $item->id,
                    'account' => $item->account->id,
                    'authorized' => $item->authorized->id,
                    'distribution' => $item->distribution->id,
                    'user' => $item->user->id,
                    'evidence' => $item->evidence,
                    'amount' => $item->amount,
                    'status' => $item->status,
                    'transfer' => $item->transfer,
                    'source' => $item->source,
                    'object' => $item->object,
                    'subject' => $item->subject,
                    'note' => $item->note,
                    'description' => $item->description,
                    'comment' => $item->comment,
                    'ctime' => $item->ctime,
                ];
            }
        }
        else if ($transaction instanceof \Gini\ORM\Object) {
            $response = [
                'id' => $transaction->id,
                'account' => $transaction->account->id,
                'authorized' => $transaction->authorized->id,
                'distribution' => $transaction->distribution->id,
                'user' => $transaction->user->id,
                'evidence' => $transaction->evidence,
                'amount' => $transaction->amount,
                'status' => $transaction->status,
                'transfer' => $transaction->transfer,
                'source' => $transaction->source,
                'object' => $transaction->object,
                'subject' => $transaction->subject,
                'note' => $transaction->note,
                'description' => $transaction->description,
                'comment' => $transaction->comment,
                'ctime' => $transaction->ctime,
            ];
        }
        
        $res = \Gini\IoC::construct('\Gini\CGI\Response\JSON', $response);
        $logger = \Gini\IoC::construct('\Gini\Logger', 'transaction');
        $logger->info('方法 [GET] 返回结果 {res}',[
            'res' => J($response)
        ]);
        return $res;
    }

    function postDefault () {
        $form = $this->form('post');
        
        $response = [
            'error' => [
                'code' => '400',
                'message' => 'Bad Request',
            ]
        ];
        
        if ($form) {
            $transaction = a('transaction');
            if ($form['authorized']) {
                $authorized = a('authorized', $form['authorized']);
                $transaction->account = $authorized->account;
                $transaction->authorized = $authorized;
            }
            else {
                $transaction->account = a('account', $form['account']);
            }
            $transaction->user = a('user', ['oid' => $form['user']]);
            $transaction->evidence = $form['evidence'] ? : '';
            $transaction->amount = $form['amount'];
            $transaction->status = $form['status'];
            $transaction->transfer = (int)$form['transfer'];
            $transaction->source = $form['source'];
            $transaction->object = $form['object'];
            $transaction->subject = $form['subject'];
            $transaction->note = $form['note'];
            $transaction->description = $form['description'];
            $transaction->comment = $form['comment'];

            if ($transaction->save()) {
                $freeze = m('fund')->freeze($transaction);
                if ($freeze && !$freeze['error']) {
                    $response = [
                        'id' => $transaction->id,
                        'account' => $transaction->account->id,
                        'authorized' => $transaction->authorized->id,
                        'distribution' => $transaction->distribution->id,
                        'user' => $transaction->user->id,
                        'evidence' => $transaction->evidence,
                        'amount' => $transaction->amount,
                        'status' => $transaction->status,
                        'transfer' => $transaction->transfer,
                        'source' => $transaction->source,
                        'object' => $transaction->object,
                        'subject' => $transaction->subject,
                        'note' => $transaction->note,
                        'description' => $transaction->description,
                        'comment' => $transaction->comment,
                        'ctime' => $transaction->ctime,
                    ];
                }
                else {
                    $transaction->delete();
                    $response = [
                        'error' => [
                            'code' => '500',
                            'message' => 'Internal Server Error',
                        ]
                    ];
                }
            }
            else {
                $response = [
                    'error' => [
                        'code' => '500',
                        'message' => 'Internal Server Error',
                    ]
                ];
            }
        }
        
        if ($response['error']) {
            $status = 'ERROR';
        } else {
            $status = 'SUCCESS';
        }

        $res = \Gini\IoC::construct('\Gini\CGI\Response\JSON', $response);
        $logger = \Gini\IoC::construct('\Gini\Logger', 'transaction');
        $logger->info('方法 [POST] [{status}] 返回结果 {res}',[
            'status' => $status,
            'res' => J($response)
        ]);
        return $res;
    }

    function putDefault ($id = 0) {
        parse_str(file_get_contents('php://input'), $form);
        
        $response = [
            'error' => [
                'code' => '400',
                'message' => 'Bad Request',
            ]
        ];

        $transaction = a('transaction', $id);
        if (!$transaction->id) {
            $response = [
                'error' => [
                    'code' => '404',
                    'message' => 'Not Found',
                ]
            ];
            goto output;
        }
        
        if ($form) {
            $authorized = a('authorized', $form['authorized']);
            $user = a('user', $form['user']);
            $transaction->account = $authorized->account;
            $transaction->authorized = $authorized;
            $transaction->amount = $form['amount'];
            $transaction->transfer = $form['transfer'];
            $transaction->source = $form['source'];
            $transaction->object = $form['object'];
            $transaction->subject = $form['subject'];
            $transaction->note = $form['note'];
            $transaction->description = $form['description'];
            $transaction->comment = $form['comment'];
            
            if ($transaction->save()) {
                $response = [
                    'id' => $transaction->id,
                    'account' => $transaction->account->id,
                    'authorized' => $transaction->authorized->id,
                    'distribution' => $transaction->distribution->id,
                    'user' => $transaction->user->id,
                    'evidence' => $transaction->evidence,
                    'amount' => $transaction->amount,
                    'status' => $transaction->status,
                    'transfer' => $transaction->transfer,
                    'source' => $transaction->source,
                    'object' => $transaction->object,
                    'subject' => $transaction->subject,
                    'note' => $transaction->note,
                    'description' => $transaction->description,
                    'comment' => $transaction->comment,
                    'ctime' => $transaction->ctime,
                ];
            }
            else {
                $response = [
                    'error' => [
                        'code' => '500',
                        'message' => 'Internal Server Error',
                    ]
                ];
            }
        }

        output:
        if ($response['error']) {
            $status = 'ERROR';
        } else {
            $status = 'SUCCESS';
        }

        $res = \Gini\IoC::construct('\Gini\CGI\Response\JSON', $response);
        $logger = \Gini\IoC::construct('\Gini\Logger', 'transaction');
        $logger->info('方法 [PUT] [{status}] 返回结果 {res}',[
            'status' => $status,
            'res' => J($response)
        ]);
        return $res;
    }
    
    function deleteDefault($id = 0) {
        $response = false;
        $transaction = a('transaction', $id);
        parse_str(file_get_contents('php://input'), $form);

        if ($transaction->id) {
            if ($transaction->delete()) {
                $response = true;
            }
            else {
                $response = [
                    'error' => [
                        'code' => '500',
                        'message' => 'Internal Server Error',
                    ]
                ];
            }
        }
    if ($response['error']) {
            $status = 'ERROR';
        } else {
            $status = 'SUCCESS';
        }

        $res = \Gini\IoC::construct('\Gini\CGI\Response\JSON', $response);
        $logger = \Gini\IoC::construct('\Gini\Logger', 'transaction');
        $logger->info('方法 [DELETE] [{status}] 返回结果 {res}',[
            'status' => $status,
            'res' => J($response)
        ]);
        return $res;
    }
}
