<?php

namespace Gini\Controller\CGI\Fund;

class User extends \Gini\Controller\REST {

    function getDefault ($id = 0) {
        $form = $this->form('get');
        $response = [];
        if ($id) {
            $connect = a('fund/user', $id);
        }
        else {
            $connect = those('fund/user');

            if ($form['keyword']) {
                $keyword = $form['keyword'];
                $connect->whose('fund')->isIn(
                    those('fund')->whose('dept_name')->contains($keyword)
                        ->orWhose('dept_no')->contains($keyword)
                        ->orWhose('prot_name')->contains($keyword)
                        ->orWhose('prot_no')->contains($keyword)
                );
            }

            if ($form['fund']) {
                $connect->whose('fund_id')->is($form['fund']);
            }

            if ($form['user']) {
                $user = a('user', ['oid' => $form['user']]);
                $connect->whose('user')->is($user);
            }

            if (isset($form['disable'])) {
                $connect->whose('disable')->is($form['disable']);
            }

            if (isset($form['show'])) {
                $connect->whose('show')->is($form['show']);
            }

            if ($form['sortby'] && $form['order']) {
                if (is_array($form['sortby'])) {
                    foreach ($form['sortby'] as $index => $sort) {
                        $connect->orderBy((string)$sort, $form['order'][$index]);
                    }
                }
                else {
                    $connect->orderBy((string)$form['sortby'], (string)$form['order']);
                }
            }

            $response['total'] = $connect->totalCount();
            
            if ($form['limit']) {
                list($start, $per) = $form['limit'];
                $connect->limit($start, $per);
            }
        }

        if ($connect instanceof \Gini\Those) {
            foreach ($connect as $item) {
                $response[] = [
                    'id' => $item->id,
                    'user' => $item->user->oid,
                    'fund' => $item->fund->id,
                    'disable' => $item->disable,
                    'show' => $item->show,
                    'ctime' => $item->ctime,
                ];
            }
        }
        else if ($connect instanceof \Gini\ORM\Object) {
            $response = [
                'id' => $connect->id,
                'user' => $connect->user->oid,
                'fund' => $connect->fund->id,
                'disable' => $item->disable,
                'show' => $item->show,
                'ctime' => $connect->ctime,
            ];
        }
        
        $status = '[GET]';
        $res = \Gini\IoC::construct('\Gini\CGI\Response\JSON', $response);
        $logger = \Gini\IoC::construct('\Gini\Logger', 'fund_user');
         $logger->info('方法 [{method}]  返回结果：{res}',[
            'method' => 'GET',
            'res' => J($response)
        ]);
        return $res;
    }

    function putDefault ($id = 0) {
        parse_str(file_get_contents('php://input'), $form);

        if (!$id) {
            $response = [
                'error' => [
                    'code' => '400',
                    'message' => 'Bad Request',
                ]
            ];
            goto output;
        }

        $connect = a('fund/user', $id);
        if (!$connect->id) {
            $response = [
                'error' => [
                    'code' => '404',
                    'message' => 'Not Found',
                ]
            ];
            goto output;
        }
        
        if ($form) {
            if (isset($form['disable'])) $connect->disable = $form['disable'] ? 1 : 0;
            if (isset($form['show'])) $connect->show = $form['show'] ? 1 : 0;
            if ($connect->save()) {
                $response = [
                    'id' => $connect->id,
                    'fund' => $connect->fund_id,
                    'user' => $connect->user->oid,
                    'disable' => $connect->disable,
                    'show' => $connect->show,
                    'ctime' => $connect->ctime,
                ];
            }
            else {
                $response = [
                    'error' => [
                        'code' => '500',
                        'message' => 'Internal Server Error',
                    ]
                ];
            }
        }

        output:
        if ($response['error']) {
            $status = 'ERROR';
        } else {
            $status = 'SUCCESS';
        }

        $res = \Gini\IoC::construct('\Gini\CGI\Response\JSON', $response);
        $logger = \Gini\IoC::construct('\Gini\Logger', 'fund_user');
        $logger->info('方法 [PUT] [{status}] 返回结果 {res}',[
            'status' => $status,
            'res' => J($response)
        ]);
        return $res;
    }

}