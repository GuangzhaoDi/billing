<?php

namespace Gini\Controller\CLI;

class Distribution extends \Gini\Controller\CLI {

    function actionAuto () {
        $now = date('Y-m-d H:i:s');
        $config = \Gini\Config::get('billing.distribution');
        $admin = $config['admin'];
        $school = $config['school'];
        
        $equipments = those('transaction')->whose('ctime')->isLessThanOrEqual($now)
            ->andWhose('distribution_id')->is(null)
            ->andWhose('subject')->isNot('')
            ->andWhose('subject')->isNot(null)->get('subject');
        $equipments = array_filter(array_unique($equipments));

        foreach ($equipments as $equipment) {
            list($object, $id) = explode('#', $equipment);
            if ($object == 'equipment') {
                $distribution = a('distribution');
                $distribution->admin = implode('-', 
                [$admin['depart'], $admin['project'], $admin['subject'], $admin['economy']]); 
                $distribution->admin_scale = $admin['scale'];
                //$distribution->manager = '104290-20600907';
                $distribution->manager_scale = $config['manager']['scale'];
                $distribution->school = implode('-', 
                [$school['depart'], $school['project'], $school['subject'], $school['economy']]); 
                $distribution->school_scale = $school['scale'];
                $distribution->equipment = $id;
                if ($distribution->save()) {
                    $db = \Gini\Database::db();
                    $db->query('UPDATE `transaction` SET `distribution_id` = :distribution
                    WHERE `ctime` <= :ctime 
                    AND `distribution_id` IS NULL 
                    AND `subject` = :equipment', null, [
                        ':ctime' => $now,
                        ':equipment' => $equipment,
                        ':distribution' => $distribution->id, 
                    ]);

                    $config = \Gini\Config::get('billing.transaction');
                    $transactions = those('transaction')->whose('distribution')->is($distribution);
                    $amount = [];
                    foreach ($transactions as $transaction) {
                        $amount[] = [
                            'amount' => $transaction->amount,
                            'note' => $transaction->note,
                        ];
                    }
                    $income = abs(array_sum(array_column($amount, 'amount')));
                    $note = abs(array_sum(array_column($amount, 'note')));
                    $distribution->balance = $income + $note;
                    $distribution->save();
                }
            }
        }
    }

    function actionStatus () {
        $distributions = those('distribution')->whose('reserve')->isNot('');
        foreach ($distributions as $distribution) {
            $results = m('fund')->status($distribution);
            foreach ($results as $result) {
                $no = str_replace('LS', '', $result['ZFLSH']);;
                $status = $result['ZT'];

                if ($status == \Gini\Model\Fund::PAY_STATUS_SUCCESSED) {
                    $db = \Gini\Database::db();
                    $db->query('UPDATE `transaction` 
                    SET `status` = :status
                    `evidence` = :evidence
                    WHERE `distribution_id` = :distribution 
                    AND `account_id` = :account', null, [
                        ':status' => \Gini\ORM\Transaction::STATUS_PAID,
                        ':evidence' => $result['PZBH'],
                        ':account' => $no,
                        ':distribution' => $distribution->id, 
                    ]);
                }
            }
        }
    }
    function actionUnfreeze () {
        $distributions = a('account')->whose('id')->is('34');
                $account = [];
        $account['BMBH'] = $distributions->fund->dept_no;
        error_log($distributions->fund->dept_no);
        $account['XMBH'] = $distributions->fund->prot_no;
        $account['JE']   = -103;//$distributions->balance;
        $account['LSH']  = 'LS'. $distributions->id;
        $account['GZH'] = $distributions->lab->owner_id;
        $account['RQ'] = date('Y-m-d', strtotime($distributions->ctime));
        $account['GZH'] = $distributions->lab->owner_id;
        $account['BZ'] = '�~M~U�~]�解�~F�';
        $account['XTLB'] = 2;
        $results = m('fund')->unfreeze_one([$account]);
        if ($results == 1) {

        }

        print_r($results);
    }

}
