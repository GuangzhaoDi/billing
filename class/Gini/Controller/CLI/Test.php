<?php

namespace Gini\Controller\CLI {

    class Test extends \Gini\Controller\CLI {

        function actionDeposit() {
            $user = a('user', 1);
            $res = m('fund')->get('wa')[2];
            $fund = a('fund')->whose('dept_no')->is($res['BMBH'])
                ->andWhose('prot_no')->is($res['XMBH']);
            
            $fund->dept_name = $res['BMMC'];
            $fund->dept_no = $res['BMBH'];
            $fund->prot_name = $res['XMMC'];
            $fund->prot_no = $res['XMBH'];
            if ($fund->save()) {
                $balance = a('account');
                $balance->fund = $fund;
                $balance->user = $user;
                $balance->lab = $user->lab;
                $balance->balance = 30000;
                $balance->save();
            }
        }

        function actionD () {
            $a = a('distribution', 1);
            m('fund')->distribute($a);
        }

        function actionBalance () {
            $funds = m('fund')->get('20063528');
            foreach ($funds as $key => $fund) {
                $balance = m('fund')->balance('20063528', $fund['BMBH'], $fund['XMBH']);
                if ($balance >= 0) {
                    m('fund')->freeze(false, [[
                        "BMBH" => $fund['BMBH'],
						"XMBH" => $fund['XMBH'],
						"JE" => 1,
                        "LSH" => 'LS' . time(),
						"GZH" => '20063528',
						"RQ" => date('Y-m-d'),
						"BZ" => '',
						"XTLB" => '1',
                    ]]);
                }
            }
        }

    }
}