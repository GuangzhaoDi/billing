<?php

namespace Gini\Model;

class Fund {
	public $soapObject;

    /*
        根据配置初始化Fund：
		配置文件
			soap_using:对接的第三方财务系统
				name:  对应 soap.{name} 
  				class: \Gini\Model\Soap\{class}
    */
	function __construct() {
        $using_soap = \Gini\Config::get("soap.soap_using");
        if (!$using_soap || !is_array($using_soap)) {
            return false;
        }
		//第三方财务名，需对应配置文件
        $soap_name = $using_soap['name'];
		$soap_class = $using_soap['class'];
		$this->soapObject = \Gini\IoC::construct("Gini\Model\Soap\\$soap_class", $soap_name);
	}

	public function __call($method, $params) {
        if ($method == __FUNCTION__) return FALSE;

        try {
            if (method_exists($this, $method)) {
                $return = call_user_func_array([$this, $method], $params);
            }
            else {
                $return = call_user_func_array([$this->soapObject, $method], $params);
            }

            return $return;
        }
        catch(\Exception $e) {
            \Gini\Logger::of('fund')
            ->debug("[ERROR]FUND Call Error => \n{error}\n", [
                'error' => $e->getMessage(),
            ]);
			return false;
        }
    }
}
