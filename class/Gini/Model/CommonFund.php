<?php

namespace Gini\Model;

abstract class CommonFund extends \Gini\SOAP {
    
    abstract function get ($ref);

    abstract function balance ($ref, $dept, $prot);
    
    abstract function freeze ($transaction, $array = []);

    abstract function distribute ($distribution);

    abstract function status ($distribution);
	
}