<?php

namespace Gini\Model\Soap;

class Szht extends \Gini\Model\CommonFund  {

	protected $getClient;

	function __construct($soapname) {
		$this->getClient = \Gini\Soap::of($soapname)->getClient();
	}

	private $_type = '1';//天财需要标志的系统类型 大仪目前默认为1

	//支付状态返回值：审核未通过
	const PAY_STATUS_APPROVAL_FAILED = 0;
	//支付状态返回值：等待审核中
	const PAY_STATUS_PENDING_APPROVAL = 6;
	//支付状态返回值：审核通过
	const PAY_STATUS_APPROVAL_SUCCESSED = 5;
	//支付状态返回值：审核通过并提走数据
	const PAY_STATUS_APPROVAL_OUT = 2;
	//支付状态返回值：支付成功
	const PAY_STATUS_SUCCESSED = 3;
	//支付状态返回值：支付失败
	const PAY_STATUS_FAILED = 4;
	//支付金额不一致
	const PAY_STATUS_CREDIT_INCONSISTENT = 7;
	
	public function get ($ref) {
		$res = $this->getClient->GetFzrxmxx(['ygbh' => (string)$ref])->GetFzrxmxxResult;
		//$res = '[{"BMBH":"021700", "BMMC":"财务处","XMBH":"0220004","XMMC":"工信监管"},{"BMBH":"021700", "BMMC":"财务处","XMBH":"302410","XMMC":"科研项目"},{"BMBH":"021700", "BMMC":"财务处","XMBH":"612001","XMMC":"结算中心业务费"}]';
		switch ($res) {
			case 'error1':
				//没有要查询的结果
				$response = ['error' => ['code' => '404', 'message' => '']];
				break;
			case 'error0':
				//没有权限
				$response = ['error' => ['code' => '401', 'message' => '']];
				break;
			default:
				$response = json_decode($res, true);
				break;
		}

		if (!isset($response['error'])) foreach ($response as $item) {
			$user = a('user', ['ref' => $ref]);
			if (!$user->id) break;
			$fund = a('fund', ['dept_no' => $item['BMBH'], 'prot_no' => $item['XMBH']]);
			if (!$fund->id) {
				$fund->dept_no = $item['BMBH'];
				$fund->dept_name = $item['BMMC'];
				$fund->prot_no = $item['XMBH'];
				$fund->prot_name = $item['XMMC'];
				$fund->save();
			}

			$fund->connect($user);
		}
		
		return $response;
	}

	public function balance ($ref, $dept, $prot) {
		$res = $this->getClient->GetXmye([
			'str_bmbh' => (string)$dept, 
			'str_xmbh' => (string)$prot, 
			'fzrbh' => (string)$ref
		])->GetXmyeResult;
		//$res = '40000';
		
		switch ($res) {
			case 'error0':
				//您没有权限调用此服务！
			case 'error1':
				//这个员工不是报账系统中该项目的负责人或被授权人
				$response = ['error' => ['code' => '401', 'message' => '']];
				break;
			case 'error2':
				//项目未设置报账分类
				$response = ['error' => ['code' => '403', 'message' => '']];
				break;
			case '-999999999':
				$response = ['error' => ['code' => '404', 'message' => '']];
				break;
			default:
				$response = (int)$res;
				break;
		}

		return $response;
	}

    public function unfreeze ($accounts) {
        $total = count($accounts);
        $root = $accounts;

        $data = ['total' => $total, 'root' => $root];
        $j_data = J($data);
        $res = $this->getClient->Jdjf([
                'json' => $j_data,
                'xtlx' => 2
        ]);
        $res = $res->JdjfResult;
        switch ($res) {
                case 'error9':
                        //�~U��~M��~S�~T~Y误
                        $response = ['error' => ['code' => '500', 'message' => '']];
                        break;
                case 'error3':
                        //json�| ��~O�~W��~X
                        $response = ['error' => ['code' => '400', 'message' => '']];
                        break;
                case 'error0':
                        //没�~\~I�~]~C�~Y~P
                        $response = ['error' => ['code' => '401', 'message' => '']];
                        break;
                case '1':
                        $response = true;
                        break;
                default:
                        $response = false;
                        break;
        }

        return $response;
    }

	public function freeze ($transaction, $array = []) {
		//if (!$transaction->transfer) return true; 
		if (!$array) {
			if ($transaction instanceof \Gini\ORM\Object) {
				if ($transaction->object == 'subsidy') {
					$config = \Gini\Config::get('billing.distribution')['subsidy'];
					$array[] = [
						"BMBH" => $config['depart'],
						"XMBH" => $config['project'],
						"JE" => $transaction->amount,
						"LSH" => 'LST' . $transaction->id,
						"GZH" => $transaction->user->ref,
						"RQ" => date('Y-m-d'),
						"BZ" => $transaction->description,
						"XTLB" => $this->_type,
					];
				}
				else {
					$array[] = [
						"BMBH" => $transaction->account->fund->dept_no,
						"XMBH" => $transaction->account->fund->prot_no,
						"JE" => $transaction->amount,
						"LSH" => $transaction->account->serialNo(),
						"GZH" => $transaction->user->ref,
						"RQ" => date('Y-m-d'),
						"BZ" => $transaction->description,
						"XTLB" => $this->_type,
					];
				}
			}
			else {
				foreach ($transaction as $item) {
					$array[] = [
						"BMBH" => $item->account->fund->dept_no,
						"XMBH" => $item->account->fund->prot_no,
						"JE" => $item->amount,
						"LSH" => $item->account->serialNo(),
						"GZH" => $item->user->ref,
						"RQ" => date('Y-m-d'),
						"BZ" => $item->description,
						"XTLB" => $this->_type,
					];
				}
			}
		}

		$request = [
			'total' => count($array),
			'root' => $array,
		];

		$res = $this->getClient->Djjf([
			'json' => J($request),
			'xtlx' => $this->_type
		])->DjjfResult;
		//$res = '1';
		switch ($res) {
			case 'error9':
				//数据库错误
				$response = ['error' => ['code' => '500', 'message' => '']];
				break;
			case 'error3':
				//json格式问题
				$response = ['error' => ['code' => '400', 'message' => '']];
				break;
			case 'error0':
				//没有权限
				$response = ['error' => ['code' => '401', 'message' => '']];
				break;
			case '1':
				$response = true;
				break;
			default:
				$response = false;
				break;
		}
		
		return $response;
	}

	public function distribute ($distribution) {
		$array = [];
		$user = $distribution->complete;
		list($admin_dept, $admin_prot, $admin_sub, $admin_econ) = explode('-', $distribution->admin);
		list($manager_dept, $manager_prot, $manager_sub, $manager_econ) = explode('-', $distribution->manager);
		list($school_dept, $school_prot, $school_sub, $school_econ) = explode('-', $distribution->school);

		$transactions = those('transaction')->whose('distribution')->is($distribution);
		$note = 0;
		$serials = [];
		foreach ($transactions as $transaction) {
			if ($transaction->object == 'subsidy') {
				$no = 'LST' . $transaction->id;
				$config = \Gini\Config::get('billing.distribution')['subsidy'];
				$serials[$no]['ZFLSH'] = $no;
				$serials[$no]['ZCBMBH'] = $config['depart'];
				$serials[$no]['ZCXMBH'] = $config['project'];
				$serials[$no]['GZH'] = $transaction->user->ref;
				$serials[$no]['XM'] = $transaction->user->name;
				$serials[$no]['JE'] = abs($transaction->amount);
				$serials[$no]['BZ'] = $transaction->description;
				continue;
			}
			$no = $transaction->account->serialNo();
			$serials[$no]['ZFLSH'] = $no;
			$serials[$no]['ZCBMBH'] = $transaction->account->fund->dept_no;
			$serials[$no]['ZCXMBH'] = $transaction->account->fund->prot_no;
			$serials[$no]['GZH'] = $transaction->user->ref;
			$serials[$no]['XM'] = $transaction->user->name;
			$serials[$no]['JE'] += abs($transaction->amount);
			$serials[$no]['BZ'] = $transaction->description;
		}
		$array['root_j'] = array_values($serials);
		$array['total_j'] = count($array['root_j']);
		$array['total_d'] = 3;
		$array['root_d'][] = [
			'ZRBMBH' => $admin_dept,
			'ZRXMBH' => $admin_prot,
			'ZRZKM' => $admin_sub,
			'ZRJJFL' => $admin_econ,
			'JE' => $distribution->balance * $distribution->admin_scale
		];
		$array['root_d'][] = [
			'ZRBMBH' => $manager_dept,
			'ZRXMBH' => $manager_prot,
			'ZRZKM' => $manager_sub,
			'ZRJJFL' => $manager_econ,
			'JE' => $distribution->balance * $distribution->manager_scale
		];
		$array['root_d'][] = [
			'ZRBMBH' => $school_dept,
			'ZRXMBH' => $school_prot,
			'ZRZKM' => $school_sub,
			'ZRJJFL' => $school_econ,
			'JE' => $distribution->balance * $distribution->school_scale
		];

		$json = J($array);
		$res = $this->getClient->PTSJ_PL([
			'str_json' => $json, 
			'xtlx' => $this->_type, 
			'fzrbh' => $user->ref
		])->PTSJ_PLResult;
		
		switch ($res) {
			case 'error1':
				//未设置商城借方经济分类
				$response = ['error' => ['code' => '502', 'message' => '']];
				break;
			case 'error2':
				//项目不存在
				$response = ['error' => ['code' => '404', 'message' => '']];
				break;
			case 'error3':
				//该报账分类没有设置主科目
				$response = ['error' => ['code' => '502', 'message' => '']];
				break;
			case 'error4':
				//未设置商城贷方科目
				$response = ['error' => ['code' => '502', 'message' => '']];
				break;
			case 'error5':
				//未设置商城贷方结算方式
				$response = ['error' => ['code' => '502', 'message' => '']];
				break;
			case 'error6':
				//写入数据库,提交失败
				$response = ['error' => ['code' => '500', 'message' => '']];
				break;
			case 'error0':
				//未认证用户
				$response = ['error' => ['code' => '401', 'message' => '']];
				break;
			default:
				$response = $res;
				break;
		}
		
		return $response;
	}

	public function status ($distribution) {
		$res = $this->getClient->GetZFZT_BYYYDH([
			'str_yydh' => $distribution->reserve, 
			'str_xtlb' => $this->_type,
		])->GetZFZT_BYYYDHResult;
		
		switch ($res) {
			case 'error0':
				//项目不存在
				$response = ['error' => ['code' => '401', 'message' => '']];
				break;
			case 'error1':
				//未设置商城借方经济分类
				$response = ['error' => ['code' => '404', 'message' => '']];
				break;
			default:
				$response = json_decode($res, true);
				break;
		}
		
		return $response;
	}
	
}