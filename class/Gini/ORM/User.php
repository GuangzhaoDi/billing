<?php

namespace Gini\ORM;

class User extends Billing\Object
{
    public $name            = 'string:50';
    public $name_abbr       = 'string:100';
    public $username        = 'string:120';
    public $isadmin         = 'bool';
    public $isowner         = 'bool';
    public $ischarger       = 'bool';
    public $card            = 'string:50';
    public $ref             = 'string:10';
    public $email           = 'string:120';
    public $phone           = 'string:120';
    public $group           = 'array';
    public $lab             = 'object:lab';
    public $ctime           = 'datetime';
    public $lastSyncTime    = 'int';
    public $oid             = 'int';
    public $oname           = 'string:50';

    protected static $db_index = [
        'unique:username',
        'unique:oid',
        'name_abbr', 'ctime',
        'ref', 'lab'
    ];

    public static $_RPCs;

    static function user_ACL($e, $user, $action, $object, $when, $where) {
        switch ($action) {
            case '授权':
                return TRUE;
                if ($user->isPi()) {
                    return TRUE;
                }
                return FALSE;
                break;
            case '充值':
                return TRUE;
                if ($user->isPi()) {
                    return TRUE;
                }
                return FALSE;
                break;
                
        }
        return $e->pass();
    }

    public function isAdmin() {
        return $this->isadmin;
    }

    public function isPi() {
        return $this->isowner;
    }

    public function isCharger() {
        return $this->ischarger;
    }

}
