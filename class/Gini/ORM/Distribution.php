<?php

namespace Gini\ORM;

class Distribution extends Object
{
    public $admin           = 'string:100'; //最高权限分配账号(设备处)
    public $admin_scale     = 'double';
    public $manager         = 'string:100'; //次级权限分配账号(院级)
    public $manager_scale   = 'double';
    public $school          = 'string:100'; //学校分配账号 
    public $school_scale    = 'double';
    public $balance         = 'double';
    public $equipment       = 'int';
    public $status          = 'int,default:1';
    public $reviewe         = 'object:user';
    public $complete        = 'object:user';
    public $reserve         = 'string:50';
    public $ctime           = 'datetime';

    protected static $db_index = [
        'equipment', 'status'
    ];

    const STATUS_PREPARE = 1; //等待确认
    const STATUS_REVIEWE = 2; //机主确认
    const STATUS_COMPLETE = 3; //补充授权
    const STATUS_ASSIGNED = 4; //已分配

    public static $STATUS_LABEL = [
        self::STATUS_PREPARE => '待确认',
        self::STATUS_REVIEWE => '待设备处确认',
        self::STATUS_COMPLETE => '确认完成',
        self::STATUS_ASSIGNED => '分配完成',
    ];

    public function save() {
        if ($this->ctime == '0000-00-00 00:00:00' || !isset($this->ctime)) $this->ctime = date('Y-m-d H:i:s');

        if ($this->status == self::STATUS_COMPLETE && !$this->reserve) {
            $this->subsidy(); 
            $this->reserve = m('fund')->distribute($this);
        }
        return parent::save();
    }

    public function subsidy () {
		$note = array_sum(those('transaction')->whose('distribution')->is($this)->get('note'));
        $transaction = a('transaction')->whose('distribution')->is($this)
            ->andWhose('object')->is('subsidy')
            ->andWhose('transfer')->is(1);

        if (!$transaction->id) {
            $transaction->distribution = $this;
            $transaction->user = $this->reviewe;
            $transaction->amount = $note; //金额
            $transaction->transfer = 1; //是否为转账
            $transaction->source = 'szht'; //费用来源
            $transaction->object = 'subsidy'; //关联对象
            $transaction->description = '补助用金额';
            $transaction->save();

            m('fund')->freeze($transaction);
        }
    }

}