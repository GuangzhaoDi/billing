<?php

namespace Gini\ORM;

class Department extends Object
{
    public $name        = 'string:100';
    public $nickname    = 'string:100'; 
    public $ctime       = 'datetime';

    protected static $db_index = [
        'name', 'nickname',
    ];

    public function save() {
        if ($this->ctime == '0000-00-00 00:00:00' || !isset($this->ctime)) $this->ctime = date('Y-m-d H:i:s');
        return parent::save();
    }

}