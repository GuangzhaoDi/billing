<?php

namespace Gini\ORM\Authorized;

class Record extends \Gini\ORM\Object
{
    public $authorized  = 'object:authorized';
    public $operator    = 'object:user';
    public $licensor    = 'object:user';
    public $balance     = 'double,default:0';
    public $type        = 'int';
    public $ctime       = 'datetime';

    protected static $db_index = [
        'operator', 'licensor',
        'type', 'authorized',
    ];
    
    const TYPE_ALLOW = 1; //允许
    const TYPE_REJECT = 2; //拒绝
    const TYPE_PUT = 3; //补充授权
    const TYPE_DEDUCT = 4; //扣除授权

    public static $types = [
        self::TYPE_ALLOW => '授权',
        self::TYPE_REJECT => '取消',
        self::TYPE_PUT => '充值',
        self::TYPE_DEDUCT => '扣费',
    ];

    public function save() {
        if ($this->ctime == '0000-00-00 00:00:00' || !isset($this->ctime)) $this->ctime = date('Y-m-d H:i:s');
        if (parent::save()) {
            return $this->authorized->update();
        }
    }

}