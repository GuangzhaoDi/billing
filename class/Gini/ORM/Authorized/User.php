<?php

namespace Gini\ORM\Authorized;

class User extends \Gini\ORM\Object
{
    public $user        = 'object:user';
    public $authorized  = 'object:authorized';
    public $ctime       = 'datetime';

    protected static $db_index = [
        'user', 'authorized',
    ];

    public function save() {
        if ($this->ctime == '0000-00-00 00:00:00' || !isset($this->ctime)) $this->ctime = date('Y-m-d H:i:s');
        $me = _G('ME');

        $record = a('authorized/record');
        $record->operator = $me;
        $record->licensor = $this->user;
        $record->balance = $this->authorized->balance;
        $record->authorized = $this->authorized;
        $record->type = \Gini\ORM\Authorized\Record::TYPE_ALLOW;
        if ($record->save()) {
            return parent::save();
        }
        else {
            $record->delete();
        }

        return false;
    }

    public function delete() {
        $me = _G('ME');

        $record = a('authorized/record');
        $record->authorized = $this->authorized;
        $record->operator = $me;
        $record->licensor = $this->user;
        $record->balance = $this->authorized->balance;
        $record->type = \Gini\ORM\Authorized\Record::TYPE_REJECT;
        if ($record->save()) {
            if (parent::delete()) {
                return true;
            }
            else {
                $record->delete();
            }
        }

        return false;
    }

}