<?php

namespace Gini\ORM\Fund;

class User extends \Gini\ORM\Billing\Object
{
    public $user        = 'object:user';
    public $fund        = 'object:fund';
    public $disable     = 'int';
    public $show        = 'int';
    public $ctime       = 'datetime';

    protected static $db_index = [
        'user', 'fund',
        'show', 'disable',
    ];

    public function save () {
        if ($this->ctime == '0000-00-00 00:00:00' || !isset($this->ctime)) $this->ctime = date('Y-m-d H:i:s');
        return parent::save();
    }
}