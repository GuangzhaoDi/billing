<?php

namespace Gini\ORM;

class Account extends Billing\Object
{
    public $fund        = 'object:fund';
    public $user        = 'object:user';
    public $lab         = 'object:lab';
    public $balance     = 'double,default:0';
    public $total       = 'double,default:0';
    public $ctime       = 'datetime';

    protected static $db_index = [
        'user', 'lab',
        'balance', 'fund',
    ];

    public function save () {
        if ($this->ctime == '0000-00-00 00:00:00' || !isset($this->ctime)) $this->ctime = date('Y-m-d H:i:s');
        return parent::save();
    }

    public function update () {
        $deposit = array_sum(those('transaction')->whose('account')->is($this)
            ->andWhose('authorized_id')->is(null)->get('amount'));
            
        $balance = array_sum(those('authorized')->whose('account')->is($this)->get('balance'));
        
        $use = array_sum(those('transaction')->whose('account')->is($this)
            ->andWhose('authorized_id')->isNot(null)->get('amount'));

        $this->total = $deposit + $use;
        $this->balance = $deposit - $balance + $use;
        return $this->save();
    }

    public function serialNo () {
        return 'LS' . $this->id;
    }

}