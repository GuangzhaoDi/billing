<?php

namespace Gini\ORM;

class Fund extends Object
{
    public $dept_name   = 'string:100';
    public $dept_no     = 'string:100';
    public $prot_name   = 'string:100';
    public $prot_no     = 'string:100';
    public $ctime       = 'datetime';

    protected static $db_index = [
        'dept_name', 'dept_no', 
        'prot_no', 'prot_name'
    ];

    public function save() {
        if ($this->ctime == '0000-00-00 00:00:00' || !isset($this->ctime)) $this->ctime = date('Y-m-d H:i:s');
        return parent::save();
    }

    public function connect ($user) {
        if ($user->id) {
            $connect = a('fund/user')->whose('user')->is($user)
                ->whose('fund')->is($this);
            if ($connect->id) {
                return true;
            }
            else {
                $connect->user = $user;
                $connect->fund = $this;
                return $connect->save();
            }
        }
        return false;
    }

}