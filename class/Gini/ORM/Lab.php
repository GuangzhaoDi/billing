<?php

namespace Gini\ORM;

class Lab extends Object
{
    public $name            = 'string:50';
    public $name_abbr       = 'string:100';
    public $owner           = 'object:user';
    public $contact         = 'string:120';
    public $ctime           = 'datetime';
    public $group           = 'array';
    public $lastSyncTime    = 'int';
    public $oid             = 'int';
    public $oname           = 'string:50';

    protected static $db_index = [
        'owner',
        'name',
        'ctime',
        'oid'
    ];

    public function save() {
        if ($this->ctime == '0000-00-00 00:00:00' || !isset($this->ctime)) $this->ctime = date('Y-m-d H:i:s');
        return parent::save();
    }

    private static $_RPCs;
    public function getRPC() {
        class_exists('\Gini\RPC');
        $username = $this->username;
        list($username, $key) = \Gini\Auth::parseUserName($username);
        
        if ($key) {
            if (!preg_match('/(.+)%([^%]+)$/', $key, $parts)) {
                throw new \Gini\RPC\Exception("未知服务器: $key");
            }
            $backend = \Gini\Auth::backends()[$key];
            $id = $backend['info_server'] ? : $parts[2];
            if (!$id) {
                throw new \Gini\RPC\Exception("无法验证服务器");
            }
        }
        else {
            $id = array_keys(\Gini\Config::get('server.remote'))[0];
        }
        
        if (!isset(self::$_RPCs[$id])) {
            $server = \Gini\Module\BillingSwu::remoteServer($id);
            $client = \Gini\Config::get('rpc.clients')['labs'];
            $rpc = \Gini\IoC::construct('\Gini\RPC', $server['api']);
            
            $sign = $rpc->lab->authorize($client['client_id'], $client['client_secret']);
            if (!$sign) {
                throw new \Gini\RPC\Exception("无法验证服务器: $id");
            }

            self::$_RPCs[$id] = $rpc;
        }

        return self::$_RPCs[$id];
    }

    //更新实验室的信息
    public function updateInfo()
    {
        $id = $this->oid;
        $rpc = $this->getRPC();
        $info = $rpc->lab->getLab($id);

        $this->name = $info['name'];
        $this->contact = $info['contact'];
        $this->group = $info['group'];
        $this->ctime = $info['ctime'];
        $this->projects = $info['projects'];
        $this->lastSyncTime = time();
        $this->oid = (int)$info['id'];
        $this->oname = $info['source'];
        $this->save();
    }

}