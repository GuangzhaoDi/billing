<?php

namespace Gini\ORM;

class Authorized extends Billing\Object
{
    public $account     = 'object:account';
    public $balance     = 'double,default:0';
    public $type        = 'int,default:2';
    public $ctime       = 'datetime';

    protected static $db_index = [
        'account', 'balance', 'type'
    ];

    const TYPE_AUTO = 1; //自动授权
    const TYPE_MANUAL = 2; //手动授权

    public static $types = [
        self::TYPE_AUTO => '自动授权',
        self::TYPE_MANUAL => '手动授权',
    ];

    public function save() {
        if ($this->ctime == '0000-00-00 00:00:00' || !isset($this->ctime)) $this->ctime = date('Y-m-d H:i:s');

        if (parent::save()) {
            return $this->account->update();
        }
        return false;
    }

    public function update () {
        if ($this->type == self::TYPE_AUTO) {
            return true;
        }

        $deposit = 0;
        $income = array_sum(those('authorized/record')->whose('authorized')->is($this)
            ->andWhose('type')->is(\Gini\ORM\Authorized\Record::TYPE_PUT)
            ->andWhose('licensor_id')->is(null)->get('balance'));

        $outcome = array_sum(those('authorized/record')->whose('authorized')->is($this)
            ->andWhose('type')->is(\Gini\ORM\Authorized\Record::TYPE_DEDUCT)
            ->andWhose('licensor_id')->is(null)->get('balance'));

        $deposit = $income - $outcome;

        $use = array_sum(those('transaction')->whose('authorized')->is($this)->get('amount'));

        $balance = $deposit + $use;
        $this->balance = $balance;
        return $this->save();
    }

    public function connect ($user) {
        if ($user->id) {
            $connect = a('authorized/user')->whose('user')->is($user)
                ->whose('authorized')->is($this);
            if ($connect->id) {
                return true;
            }
            else {
                $connect->user = $user;
                $connect->authorized = $this;
                return $connect->save();
            }
        }
        return false;
    }

    public function disconnect ($user) {
        if ($user->id) {
            $connect = a('authorized/user')->whose('user')->is($user)
                ->whose('authorized')->is($this);
            if (!$connect->id) return true;
            
            return $connect->delete();
        }
        return false;
    }

}
