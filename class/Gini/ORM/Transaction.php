<?php

namespace Gini\ORM;

class Transaction extends Object
{
    public $account         = 'object:account';
    public $authorized      = 'object:authorized';
    public $distribution    = 'object:distribution';
    public $user            = 'object:user'; 
    public $evidence        = 'string:100'; //凭证号
    public $amount          = 'double,default:0'; //金额
    public $status          = 'int,default:0'; //状态
    public $transfer        = 'int'; //是否为转账
    public $source          = 'string:100'; //费用来源
    public $object          = 'string:100'; //关联对象
    public $subject         = 'string:100'; //不知道叫嘛了 关联对象2
    public $note            = 'string:100'; //该字段为财务明细的补充字段,因可能有多种用途设置为string 西南大学中代表补助费
    public $description     = 'string:500';
    public $comment         = 'string:500';
    public $remark          = 'string:500';
    public $ctime           = 'datetime';

    protected static $db_index = [
        'account', 'distribution', 'user', 
        'amount', 'evidence', 'status',
        'transfer', 'source'
    ];

    const STATUS_PREPARE = 0; //待支付
    const STATUS_PAID = 1; //已支付

    public static $STATUS_LABEL = [
        self::STATUS_PREPARE => '待支付',
        self::STATUS_PAID => '已支付',
    ];
    
    public function save() {
        if ($this->ctime == '0000-00-00 00:00:00' || !isset($this->ctime)) $this->ctime = date('Y-m-d H:i:s');

        $authorized = $this->authorized;
        $account = $this->account;
        if ($this->amount != 0 && parent::save()) {
            if ($authorized->id && $authorized->type == \Gini\ORM\Authorized::TYPE_MANUAL) {
                return $authorized->update();
            }
            else {
                return $account->update();
            }
        }

        return false;
    }

    public function delete() {
        $authorized = $this->authorized;
        $account = $this->account;
        if ($this->amount != 0 && parent::delete()) {
            if ($authorized->id) {
                return $authorized->update();
            }
            else {
                return $account->update();
            }
        }
        
        return false;
    }

}