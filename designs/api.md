# 基于REST的服务（RESTful API）

## 常用HTTP动词介绍

	GET（SELECT）：从服务器取出资源 `一项或者多项`
	POST（CREATE）：在服务器新建资源。
	PUT（UPDATE）：在服务器更新资源 `客户端提供改变的完整属性`
	PATCH（UPDATE）：在服务器更新资源 `客户端提供改变的部分属性`
	DELETE（DELETE）：从服务器删除资源


## `GET   	http://Myservice/Authorized`
## `GET   	http://Myservice/Authorized/$id`
## `POST  	http://Myservice/Authorized`
## `PATCH 	http://Myservice/Authorized/$id`

## `GET   	http://Myservice/Authorized/Record`
## `GET   	http://Myservice/Authorized/Record/$id`

## `GET   	http://Myservice/Authorized/User`
## `GET   	http://Myservice/Authorized/User/$id`
## `POST  	http://Myservice/Authorized/User`
## `PATCH   http://Myservice/Authorized/User/$id`
## `DELETE  http://Myservice/Authorized/User/$id`


## `GET   	http://Myservice/Account`
## `GET   	http://Myservice/Account/$id`
## `POST  	http://Myservice/Account`
## `PATCH 	http://Myservice/Account/$id`


## `GET   	http://Myservice/Fund`
## `GET   	http://Myservice/Fund/$id`
## `POST  	http://Myservice/Fund`


## `GET   	http://Myservice/Transaction`
## `GET   	http://Myservice/Transaction/$id`
## `POST  	http://Myservice/Transaction`